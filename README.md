# Exame - Bexs DevOps

## Desafio

Considerando as aplicações presentes neste repositório detalhadas abaixo, precisamos de uma stack que permita a comunicação entre ambas e o acesso de desenvolvedores.

* **Aplicação Frontend** - Aplicação em Python com Flask expondo na porta 8000 um formulário de criação de usuário contendo os campos ID e Name que realiza uma chamada Post com tais dados para a aplicação Backend.

* **Aplicação Backend** - Aplicação em Go (Golang) expondo na porta 8080 o CRUD de Usuários e armazena em um banco Sqlite3 local.

## Como entregar sua solução?

1) Clone do repositório

2) Realize as alterações necessárias para construção/automação da stack. Considere um ambiente local (máquina do desenvolvedor) ou algum provedor de cloud (AWS ou GCP).

3) Adicione e commit todos os arquivos criados/alterados (todos mesmo)

4) Gere um patch conforme comando de exemplo abaixo

5) Nos envie o patch através do email que entramos em contato

*Para gerar o patch:*
```
git format-patch origin/master --stdout > seu_nome.patch
```
## Requisitos

* Não publique sua solução. Apenas nos envie o que desenvolveu.
* Crie imagens Docker para ambas as aplicações. 
* Preencher este arquivo README.md com os detalhes, linha de raciocínio e dicas para os desenvolvedores que utilizarão sua solução.
* Considere que os desenvolvedores estão iniciando carreira e precisarão de mais detalhes de como executar sua solução.
* A Stack pode usar os recursos do próprio desenvolvedor(ex. VirtualBox, Docker, Docker-Compose) ou recursos de um provedor de cloud (Amazon Web Service ou Google Cloud)
* Não é necessário a criação de um pipeline. Considere que sua solução fará o bootstrap da Stack em questão.
* Não se preocupe em montar uma solução complexa. Dê preferência em montar uma solução simples que permita que o desenvolvedor realize melhorias.
* Apresente um desenho macro de arquitetura de sua solução.

## Bonus

* Sinta-se a vontade para realizar melhorias no código das aplicações, caso julgue necessário.

## Dúvidas

Entre em contato e nos questione.

## **Implementação da stack de desenvolvimento**

### STACK DEVOPS BEXS

### Tecnologias utilizada no EXAM

**Backend:**

   * [Golang](https://tip.golang.org/doc/go1.18)

   * [Banco de dados SQLite3](https://www.sqlite.org/about.html)


**Frontend:**
    
   * [Python com Flask](https://flask.palletsprojects.com/en/2.1.x/)


### Pré-Requisitos para ambiente de desenvolvimento

* [docker](https://docs.docker.com/engine/install/ubuntu/)

    Docker é um conjunto de produtos de plataforma como serviço que usam virtualização de nível de sistema operacional para entregar software em pacotes chamados contêineres. Os contêineres são isolados uns dos outros e agrupam seus próprios softwares, bibliotecas e arquivos de configuração

* [docker-compose](https://docs.docker.com/compose/install/)
    
    O Compose é uma ferramenta para definir e executar aplicativos Docker de vários contêineres. Com o Compose, você usa um arquivo YAML para configurar os serviços do seu aplicativo. Então, com um único comando, você cria e inicia todos os serviços da sua configuração


### Repositorio do bitbucket: *bexs-devops-exam* (branch: *master*)

    git clone https://w0liveir4@bitbucket.org/w0liveir4/bexs-devops-exam.git

### Criando as imagens no docker (backend e frontend)

**Backend desenvolvimento**

    Estrutura de diretórios
        .
        ├── /backend/src/backend
            └── main.go
            └── backend.dockerfile
        
    $ cd /backend/src/backend
    $ docker image build -t exam/backend .

Execute um contêiner com base em sua imagem


    $ docker run -d --name api-backend -p 8080:8080 exam/backend:latest

**Frontend desenvolvimento**

    Estrutura de diretórios
    .
    ├── /frontend/src/frontend
        └── frontend.py
        └── frontend.dockerfile

    $ cd /frontend/src/frontend
    $ docker image build -t exam/frontend .

Execute um contêiner com base em sua imagem


    $ docker run -d --name app-frontend -p 8000:8000 exam/frontend:latest



***Deploy de desenvolvimento com Docker-compose***

    Estrutura de diretórios
    .
    ├── .
        └── docker-compose.yaml

    
    $ docker-compose up -d --build

Para verificar os logs das APIs, execute:

    $ docker logs -f --tail 1000 ${name_container}



***URLs de desenvolvimento Local***

    Frontend: http://localhost:8000

    Backend: http://localhost/8080/


***Testando a API utilizando Postman ou Insomia -> (GET, POST...)***

    Exemplo de listagem: GET http://localhost:8080/users


#### Canditado:  Wagner Batista de Oliveira ==> [Linkdin](https://www.linkedin.com/in/wagner-oliveira-61435929/)

#### Telefone: 92 98142-7772

#### Email: gnes.oliveira@gmail.com
