FROM python:3.8-alpine

WORKDIR /app

ENV STATIC_URL /static
ENV STATIC_PATH /var/www/app/static

COPY ./requirements.txt /var/www/requirements.txt

RUN pip3 install --no-cache-dir -r /var/www/requirements.txt

COPY . .

EXPOSE 8000

CMD ["python3", "frontend.py", "runserver", "0.0.0.0:8000"]
