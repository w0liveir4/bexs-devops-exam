FROM golang:1.18
WORKDIR /usr/src/app

COPY . .
RUN go mod init backend && go mod tidy

# COPY . .
RUN go build -v -o /usr/local/bin/app ./...

EXPOSE 8080

CMD ["app"]
